## Listed Augmentation Available

Blur - blur <br/>
RBC - random brightness and contrast <br/>
GB - gaussian blur <br/>
DS - down scale <br/>
CJ - Color jitter <br/>
HF - Horizontal flip <br/>
CLAHE - Contrast Limited Adaptive Histogram Equalization <br/>
EQ - Equalizer <br/>
SP - Sharpen <br/>
VF - Vertical flip <br/>
NO - No Augmentation <br/>
<br/>
CP1 - if image is 1024 (width) x 512 (height), crop half to left side <br/>
CP2 - if image is 1024 (width) x 512 (height), crop half to right side <br/>
CP3 - if image is 512 (width) x 1024 (height), crop half to bottom side <br/>
CP4 - if image is 512 (width) x 1024 (height), crop half to upper side <br/>
<br/>
R90 - rotate 90 degree <br/>
R270 - rotate 270 degree <br/>
R45 - rotate 45 degree <br/>
R135 - rotate 135 degree <br/>
R225 - rotate 225 degree <br/>
R315 - rotate 315 degree <br/>

Execution:
`python3 aug_albumentation_common.py -i <path to dataset folder> -aug <any of above acronyms, separated with ','>`
<br/>
To install on global environment:
`pip install -U albumentations`
<br/>
To install on python3 environment:
`python3 -m pip install albumentations`
