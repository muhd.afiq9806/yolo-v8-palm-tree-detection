##### python albumentation.py -i <folder path> -aug <augmentation_class> #########
import cv2
import os
import sys
import math
import random
import argparse
import copy
import glob
import imgaug as ia
import imgaug.augmenters as iaa
import albumentations as A
from pascal_voc_writer import Writer
from xml.dom import minidom
from bbaug import policies
from argparse import ArgumentParser, SUPPRESS
from imgaug.augmentables.bbs import BoundingBox, BoundingBoxesOnImage

def build_argparser():
    parser = ArgumentParser(add_help=False)
    ap = parser.add_argument_group('Options')
    ap.add_argument("-i", "--input", type=str,
	    help="parent path to image folder")
    ap.add_argument("-aug", "--augmentation", type=str,
	    help="Augmentation list saperated by comma.",
        default = "NO")
    return parser


def readImage(imagespath, filename):
    # OpenCV uses BGR channels
    img = cv2.imread(os.path.join(imagespath,filename))
    return img


def getCoordinates(imagespath, filename):
    allbb = []
    xmldoc = minidom.parse(os.path.join(imagespath,filename))
    itemlist = xmldoc.getElementsByTagName('object')

    size = xmldoc.getElementsByTagName('size')[0]
    width = int((size.getElementsByTagName('width')[0]).firstChild.data)
    height = int((size.getElementsByTagName('height')[0]).firstChild.data)

    for item in itemlist:
        classid = (item.getElementsByTagName('name')[0]).firstChild.data
        xmin = ((item.getElementsByTagName('bndbox')[
            0]).getElementsByTagName('xmin')[0]).firstChild.data
        ymin = ((item.getElementsByTagName('bndbox')[
            0]).getElementsByTagName('ymin')[0]).firstChild.data
        xmax = ((item.getElementsByTagName('bndbox')[
            0]).getElementsByTagName('xmax')[0]).firstChild.data
        ymax = ((item.getElementsByTagName('bndbox')[
            0]).getElementsByTagName('ymax')[0]).firstChild.data

        xmin = float(xmin)
        ymin = float(ymin)
        xmax = float(xmax)
        ymax = float(ymax)

        b = [xmin, ymin, xmax, ymax, classid]
        allbb.append(b)
    return allbb

def readYolo(filename):
    coords = []
    with open(filename, 'r') as fname:
        for file1 in fname:
            x = file1.strip().split(' ')
            x.append(x[0])
            x.pop(0)
            x[0] = float(x[0])
            x[1] = float(x[1])
            x[2] = float(x[2])
            x[3] = float(x[3])
            coords.append(x)
    return coords



def writeVoc(bboxes, path_name, image):
    height, width, channels = image.shape
    base_name = os.path.splitext(path_name)[0]
    
    imagepath = path_name 
    xmlpath = f'{base_name}.xml'
    writer = Writer(imagepath, width, height)

    for i in bboxes:
        writer.addObject(i[4], float(i[0]), float(i[1]), float(i[2]), float(i[3]))
    writer.save(xmlpath)


def writeYolo(coords, path_name, name):
    base_name = os.path.splitext(path_name)[0]
    
    imagepath = path_name 
    txtpath = f'{base_name}.txt'
    with open(txtpath, "w") as f:
        for x in coords:
            f.write("%s %s %s %s %s \n" % (x[-1], x[0], x[1], x[2], x[3]))


def getTransform(imagespath, loop):
    if loop == "Blur":
        optn = "Blur"
        saved_path = os.path.join(imagespath, optn)
        os.makedirs(saved_path, exist_ok=True)
        transform = A.Compose([
            A.Blur(blur_limit=[6,7], always_apply=False, p=1.0),
        ], bbox_params=A.BboxParams(format='pascal_voc'))
    elif loop == "RBC":
        optn = "RBC"
        saved_path = os.path.join(imagespath, optn)
        os.makedirs(saved_path, exist_ok=True)
        transform = A.Compose([
            A.RandomBrightnessContrast(brightness_limit=[-0.1,0.0], contrast_limit=0.1, brightness_by_max=True, always_apply=False, p=1.0),
        ], bbox_params=A.BboxParams(format='pascal_voc'))
    elif loop == "GB":
        optn = "GB"
        saved_path = os.path.join(imagespath, optn)
        os.makedirs(saved_path, exist_ok=True)
        transform = A.Compose([
            A.GaussianBlur (blur_limit=[5,7], sigma_limit=0, always_apply=False, p=1.0),
        ], bbox_params=A.BboxParams(format='pascal_voc'))
    elif loop == "DS":
        optn = "DS"
        saved_path = os.path.join(imagespath, optn)
        os.makedirs(saved_path, exist_ok=True)
        transform = A.Compose([
            A.Downscale (scale_min=0.5, scale_max=0.5, interpolation=0, always_apply=False, p=1.0),
        ], bbox_params=A.BboxParams(format='pascal_voc'))
    elif loop == "CJ":
        optn = "CJ"
        saved_path = os.path.join(imagespath, optn)
        os.makedirs(saved_path, exist_ok=True)
        transform = A.Compose([
            A.ColorJitter (brightness=0.2, contrast=0.2, saturation=0.1, hue=0.1, always_apply=False, p=1.0),
        ], bbox_params=A.BboxParams(format='pascal_voc'))
    elif loop == "HF":
        optn = "HF"
        saved_path = os.path.join(imagespath, optn)
        os.makedirs(saved_path, exist_ok=True)
        transform = A.Compose([
            A.HorizontalFlip(p=1.0),
        ], bbox_params=A.BboxParams(format='pascal_voc'))
    elif loop == "CLAHE":
        optn = "CLAHE"
        saved_path = os.path.join(imagespath, optn)
        os.makedirs(saved_path, exist_ok=True)
        transform = A.Compose([
            A.CLAHE(clip_limit=4.0, tile_grid_size=(8, 8), always_apply=False, p=1.0),
        ], bbox_params=A.BboxParams(format='pascal_voc'))
    elif loop == "EQ":
        optn = "EQ"
        saved_path = os.path.join(imagespath, optn)
        os.makedirs(saved_path, exist_ok=True)
        transform = A.Compose([
            A.Equalize(p=1.0),
        ], bbox_params=A.BboxParams(format='pascal_voc'))
    elif loop == "SP":
        optn = "SP"
        saved_path = os.path.join(imagespath, optn)
        os.makedirs(saved_path, exist_ok=True)
        transform = A.Compose([
            A.Sharpen(alpha=(0.5, 0.7), lightness=(0.7, 1.0), p=1.0),
        ], bbox_params=A.BboxParams(format='pascal_voc'))
    elif loop == "NO":
        optn = "NO"
        saved_path = os.path.join(imagespath, optn)
        os.makedirs(saved_path, exist_ok=True)
        transform = A.Compose([
            A.NoOp(),
        ], bbox_params=A.BboxParams(format='pascal_voc'))
    elif loop == "VF":
        optn = "VF"
        saved_path = os.path.join(imagespath, optn)
        os.makedirs(saved_path, exist_ok=True)
        transform = A.Compose([
            A.VerticalFlip(p=1.0),
        ], bbox_params=A.BboxParams(format='pascal_voc'))

    # =================================== CROP =========================================================

    elif loop == "CP1":
        optn = "CP1"
        saved_path = os.path.join(imagespath, optn)
        os.makedirs(saved_path, exist_ok=True)
        transform = A.Compose([
            A.crops.transforms.Crop(x_min=0, y_min=0, x_max=640, y_max=640, always_apply=False, p=1.0),
        ], bbox_params=A.BboxParams(format='pascal_voc'))
    elif loop == "CP2":
        optn = "CP2"
        saved_path = os.path.join(imagespath, optn)
        os.makedirs(saved_path, exist_ok=True)
        transform = A.Compose([
            A.crops.transforms.Crop(x_min=680, y_min=0, x_max=1280, y_max=640, always_apply=False, p=1.0),
        ], bbox_params=A.BboxParams(format='pascal_voc'))
    elif loop == "CP3":
        optn = "CP3"
        saved_path = os.path.join(imagespath, optn)
        os.makedirs(saved_path, exist_ok=True)
        transform = A.Compose([
            A.crops.transforms.Crop(x_min=0, y_min=640, x_max=640, y_max=1280, always_apply=False, p=1.0),
        ], bbox_params=A.BboxParams(format='pascal_voc'))
    elif loop == "CP4":
        optn = "CP4"
        saved_path = os.path.join(imagespath, optn)
        os.makedirs(saved_path, exist_ok=True)
        transform = A.Compose([
            A.crops.transforms.Crop(x_min=640, y_min=640, x_max=1280, y_max=1280, always_apply=False, p=1.0),
        ], bbox_params=A.BboxParams(format='pascal_voc'))
    elif loop == "CP5":
        optn = "CP5"
        saved_path = os.path.join(imagespath, optn)
        os.makedirs(saved_path, exist_ok=True)
        transform = A.Compose([
            A.augmentations.transforms.RandomGridShuffle (grid=(4,3), always_apply=False, p=1),
        ], bbox_params=A.BboxParams(format='pascal_voc'))

    # ================================ ROTATIONS ==========================================================

    elif loop == "R90":
        optn = "R90"
        saved_path = os.path.join(imagespath, optn)
        os.makedirs(saved_path, exist_ok=True)
        transform = A.Compose([
            A.Rotate(limit=(-90,-90), interpolation=1, border_mode=0, value=None, mask_value=None, always_apply=False, p=1),
        ], bbox_params=A.BboxParams(format='pascal_voc'))
    elif loop == "R270":
        optn = "R270"
        saved_path = os.path.join(imagespath, optn)
        os.makedirs(saved_path, exist_ok=True)
        transform = A.Compose([
            A.Rotate(limit=(90,90), interpolation=1, border_mode=0, value=None, mask_value=None, always_apply=False, p=1),
        ], bbox_params=A.BboxParams(format='pascal_voc'))
    elif loop == "R45":
        optn = "R45"
        saved_path = os.path.join(imagespath, optn)
        os.makedirs(saved_path, exist_ok=True)
        transform = A.Compose([
            A.Rotate(limit=(315,315), interpolation=1, border_mode=0, value=None, mask_value=None, always_apply=False, p=1),
        ], bbox_params=A.BboxParams(format='pascal_voc'))

    elif loop == "R135":
        optn = "R135"
        saved_path = os.path.join(imagespath, optn)
        os.makedirs(saved_path, exist_ok=True)
        transform = A.Compose([
            A.Rotate(limit=(225,225), interpolation=1, border_mode=0, value=None, mask_value=None, always_apply=False, p=1),
        ], bbox_params=A.BboxParams(format='pascal_voc'))

    elif loop == "R225":
        optn = "R225"
        saved_path = os.path.join(imagespath, optn)
        os.makedirs(saved_path, exist_ok=True)
        transform = A.Compose([
            A.Rotate(limit=(135,135), interpolation=1, border_mode=0, value=None, mask_value=None, always_apply=False, p=1),
        ], bbox_params=A.BboxParams(format='pascal_voc'))

    elif loop == "R315":
        optn = "R315"
        saved_path = os.path.join(imagespath, optn)
        os.makedirs(saved_path, exist_ok=True)
        transform = A.Compose([
            A.Rotate(limit=(45,45), interpolation=1, border_mode=0, value=None, mask_value=None, always_apply=False, p=1),
        ], bbox_params=A.BboxParams(format='pascal_voc'))


    return transform, saved_path, loop


def main():
    args = build_argparser().parse_args()
    imagespath = args.input
    random.seed(7)
    title = 0
    # count = 1
    transform_list = (args.augmentation).split(',')
    print(transform_list)
    for filename in sorted(os.listdir(imagespath)):

        if filename.endswith(".jpg") or filename.endswith(".JPG") or filename.endswith(".jpeg") or filename.endswith(".png") or filename.endswith(".PNG") or filename.endswith(".tif") or filename.endswith(".TIF"):
            baseTitle, ext = os.path.splitext(os.path.basename(filename))
            image = readImage(imagespath, filename)
            title = str(baseTitle)
        if filename.endswith(".xml"):
            xmlTitle, txtExt = os.path.splitext(os.path.basename(filename))
            if xmlTitle == title:
                bboxes = getCoordinates(imagespath, filename)
                # bboxes = readYolo(imagespath+xmlTitle+'.txt')
                

                for i in range(len(transform_list)):
                    img = copy.deepcopy(image)
                    PassAugmentation = transform_list[i]
                    transform, saved_path, optn = getTransform(imagespath, PassAugmentation)
                    try:
                        transformed = transform(image=img, bboxes=bboxes)
                        transformed_image = transformed['image']
                        transformed_bboxes = transformed['bboxes']
                        name = f"{title}_{optn}.jpg"
                        # name = f"{title}_{optn}{str(count)}.jpg"
                        path_name = os.path.join(saved_path,name)
                        cv2.imwrite(path_name, transformed_image)
                        writeVoc(transformed_bboxes, path_name, transformed_image)
                        # writeYolo(transformed_bboxes, path_name, name)
                        # count = count+1
                    except:
                        print("bounding box issues")
                        pass

                # bboxes = [[int(float(j)) for j in i] for i in bb]


if __name__ == '__main__':
    sys.exit(main() or 0)