## For CSV to XML conversion
This convert a csv file data to xml file data.
Required:
1. Dataset folder including csv file inside it

Execution:
`python read_csv_xml.py -i <path to dataset folder>`

## For XML to TXT YOLO format conversion
Required:
1. Dataset folder that include images and xml files
2. label_map.txt file that use for TF training

Execution:
`python read_xml_to_txt.py -i <path to dataset folder> -pbtxt <path to pbtxt file>`