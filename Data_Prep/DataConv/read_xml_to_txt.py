##### python albumentation.py -i <folder path> -pbtxt <label_map.pbtxt path> #########
import cv2 
import os
import sys
import random
from read_labelmap_tf import read_label_map, getclassid, writeyolo_class_labels
from xml.dom import minidom
from argparse import ArgumentParser, SUPPRESS

def build_argparser():
    parser = ArgumentParser(add_help=False)
    ap = parser.add_argument_group('Options')
    ap.add_argument("-i", "--input", type=str,
	    help="parent path to image folder")
    ap.add_argument("-pbtxt", "--input_labelmap_pbtxt", type=str,
	    help="path to label_map.pbtxt file with TF format",
        default = "label_map.pbtxt")
    return parser

def copyImage(imagespath, filename, save_yolo_path):
    # OpenCV uses BGR channels
    img = cv2.imread(os.path.join(imagespath, filename))
    cv2.imwrite(os.path.join(save_yolo_path, filename), img)

def getCoordinates(imagespath, filename):
    allbb = []
    xmldoc = minidom.parse(os.path.join(imagespath,filename))
    itemlist = xmldoc.getElementsByTagName('object')

    size = xmldoc.getElementsByTagName('size')[0]
    width = int((size.getElementsByTagName('width')[0]).firstChild.data)
    height = int((size.getElementsByTagName('height')[0]).firstChild.data)

    for item in itemlist:
        classid = (item.getElementsByTagName('name')[0]).firstChild.data
        xmin = ((item.getElementsByTagName('bndbox')[
            0]).getElementsByTagName('xmin')[0]).firstChild.data
        ymin = ((item.getElementsByTagName('bndbox')[
            0]).getElementsByTagName('ymin')[0]).firstChild.data
        xmax = ((item.getElementsByTagName('bndbox')[
            0]).getElementsByTagName('xmax')[0]).firstChild.data
        ymax = ((item.getElementsByTagName('bndbox')[
            0]).getElementsByTagName('ymax')[0]).firstChild.data

        xmin = float(xmin)
        ymin = float(ymin)
        xmax = float(xmax)
        ymax = float(ymax)

        b = [xmin, ymin, xmax, ymax, classid, width, height]
        allbb.append(b)
    return allbb

def getnewbboxed(bboxes, pbtxt_input, save_yolo_path):
    label_map = read_label_map(pbtxt_input)
    key, classid_for_yolo = getclassid(label_map)
    newbboxes = []
    for a in bboxes:
        if a[4] == key:
            a[4] = classid_for_yolo
        newbboxes.append(a)
    writeyolo_class_labels(label_map, save_yolo_path)
    
    return newbboxes

def writeYolo(coords, path_name, name):
    save_path = os.path.join(path_name, name)
    txtpath = f'{save_path}.txt'

    with open(txtpath, "w") as f:
        for x in coords:
            # x[0] = xmin 
            # x[1] = ymin
            # x[2] = xmax
            # x[3] = ymax
            # x[4] = class ID 
            # x[-2] = width
            # x[-1] = height

            norm_x_center = (x[0] + ((x[2] - x[0]) / 2)) / x[-2]
            norm_y_center = (x[1] + ((x[3] - x[1]) / 2)) / x[-1]
            norm_width = (x[2] - x[0])/ x[-2]
            norm_height =  (x[3] - x[1])/ x[-1]
            f.write("%s %s %s %s %s \n" % (x[4], norm_x_center, norm_y_center, norm_width, norm_height))

def main():
    args = build_argparser().parse_args()
    imagespath = args.input
    pbtxt_input = args.input_labelmap_pbtxt
    random.seed(7)
    save_yolo_path = os.path.join(imagespath, "yolo_files")
    os.makedirs(save_yolo_path, exist_ok=True)

    for filename in sorted(os.listdir(imagespath)):
        if filename.endswith(".jpg") or filename.endswith(".JPG") or filename.endswith(".jpeg") or filename.endswith(".png") or filename.endswith(".PNG"):
            copyImage(imagespath, filename, save_yolo_path)
            
        if filename.endswith(".xml"):
            xmlTitle, xmlEXT = os.path.splitext(os.path.basename(filename))
            bboxes = getCoordinates(imagespath, filename)
            new_bboxes = getnewbboxed(bboxes, pbtxt_input, save_yolo_path)
            writeYolo(new_bboxes, save_yolo_path, xmlTitle)


if __name__ == '__main__':
    sys.exit(main() or 0)