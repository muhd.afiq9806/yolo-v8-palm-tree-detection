import cv2
import os
import sys
import random
import csv
from pascal_voc_writer import Writer
from argparse import ArgumentParser, SUPPRESS

def build_argparser():
    parser = ArgumentParser(add_help=False)
    ap = parser.add_argument_group('Options')
    ap.add_argument("-i", "--input", type=str,
	    help="parent path to image folder")
    return parser

def readCSV(filename):
    x = []
    y = []
    myorder = []
    with open(filename, 'r') as fname:
        reader = csv.reader(fname)
        # skip header
        header = next(reader)
        for i in header:
            if i == "filename":
                a = 0
            elif i == "width":
                a = 1
            elif i == "height":
                a = 2
            elif i == "class":
                a = 3
            elif i == "xmin":
                a = 4
            elif i == "xmax":
                a = 6
            elif i == "ymin":
                a = 5
            else:
                a = 7
            myorder.append(a)

        for row in reader:
            if row != None:
                mylist = sorted(zip(row, myorder), key=lambda x: x[1])
                coords = ([item[0] for item in mylist])
                nimage = f"{row[1]}"
                x.append(coords)
                y.append(nimage)
    return x, y

def writeVoc(bboxes, imagespath, imgList):
    indexList = []  
    for a in range(len(imgList)):
        indexList.append([imgList[a],a])
    indexList.sort()
    sort_img = [] #new index for sorted image list
    sort_index = [] #new index for sorted image list

    for x in indexList:
        sort_index.append(x[1])
        sort_img.append(x[0])

    # sort bboxes base on new index list 
    sort_boxes = []
    for i,x in enumerate(sort_index):
        sort_boxes.append(bboxes[x])

    same = 0
    for i,x in enumerate(sort_boxes):
        # print(x[1])
        baseTitle, ext = os.path.splitext(os.path.basename(x[0]))
        savepath = os.path.join(imagespath,baseTitle)
        xmlpath = f'{savepath}.xml'
        if same == 0:
            writer = Writer(x[0], x[1], x[2])
            writer.addObject(x[3], float(x[4]), float(x[5]), float(x[6]), float(x[7]))
            same = 1
        else:
            if i < len(sort_img)-1 and sort_img[i+1] == x[1]:
                print("same")
                writer.addObject(x[3], float(x[4]), float(x[5]), float(x[6]), float(x[7]))
                same = 1
            else:
                print('not same')
                writer.addObject(x[3], float(x[4]), float(x[5]), float(x[6]), float(x[7]))
                writer.save(xmlpath)
                same = 0

def main():
    args = build_argparser().parse_args()
    imagespath = args.input
    random.seed(7)
    for filename in sorted(os.listdir(imagespath)):
        
        if filename.endswith(".csv"):
            bboxes, imgList = readCSV(os.path.join(imagespath,filename))
            writeVoc(bboxes, imagespath, imgList)


if __name__ == '__main__':
    sys.exit(main() or 0)
