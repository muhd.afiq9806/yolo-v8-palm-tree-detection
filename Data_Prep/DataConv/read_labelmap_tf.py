import os

def read_label_map(label_map_path):

    item_id = None
    item_name = None
    items = {}
    
    with open(label_map_path, "r") as file:
        for line in file:
            line.replace(" ", "")
            if line == "item{":
                pass
            elif line == "}":
                pass
            elif "id" in line:
                item_id = int(line.split(":", 1)[1].strip())
            elif "name" in line:
                item_name = line.split(":", 1)[1].replace("'", "").strip()

            if item_id is not None and item_name is not None:
                items[item_name] = item_id
                item_id = None
                item_name = None

    return items

def getclassid(label_map):
    for key, value in label_map.items():
        classid_for_yolo = value - 1

    return key, classid_for_yolo

def writeyolo_class_labels(label_map, save_yolo_path):
    file_path = os.path.join(save_yolo_path, "classes.labels")
    with open(file_path, "w") as f:
        for key in label_map.keys():
            f.write("%s \n" % (key))
