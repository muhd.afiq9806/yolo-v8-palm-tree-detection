import cv2
import os
import numpy as np
import shutil
import argparse
from tqdm import tqdm

parser = argparse.ArgumentParser(description='Edit mobile mapping CSV file.')
parser.add_argument(
        '-f', '--folderName',
        help='Path to the inference image.',
        type=str,
        default=os.getcwd()
)


args = parser.parse_args()

#Declare variable from argparse
folderName = args.folderName


folder = folderName
# src = folderName+'/predict2/labels'
# dst = folderName+'/labels'

# shutil.move(src, dst)
file_list = os.listdir(folder)
for filename in tqdm(file_list):
#     print(filename)
    # Load the image
    image = cv2.imread(os.path.join(folder, filename))
    height, width, channel = image.shape
    imgsz = height*width
    # print('image size:'+str(imgsz))
    # Convert the image to grayscale
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    # Apply thresholding
    thresh = cv2.threshold(gray, 250, 255, cv2.THRESH_BINARY)[1]
    # Count the number of black pixels
    black_pixels = np.count_nonzero(thresh == 255)
#     print('black pixel:'+str(black_pixels))
    average=black_pixels/imgsz
    avg_int = int(average)
    # print('average:'+ str(average)+', round off:'+str(avg_int))
    # If the number of black pixels is less than a certain threshold, delete the image
    if avg_int == 1:
        # print('delete')
        os.remove(os.path.join(folder, filename))

# shutil.move(dst, folder)

print("DONE")