import os
import shutil
import sys
from argparse import ArgumentParser, SUPPRESS

def build_argparser():
    parser = ArgumentParser(add_help=False)
    ap = parser.add_argument_group('Options')
    ap.add_argument("-d", "--dataset", type=str,
	    help="parent path to image folder")
    return parser

def process(curr_dir, folder, save_labels, save_imgs):
    save_label_dir = os.path.join(save_labels, folder)
    save_imgs_dir = os.path.join(save_imgs, folder)
    os.makedirs(save_label_dir, exist_ok=True)
    os.makedirs(save_imgs_dir, exist_ok=True)

    for filename in sorted(os.listdir(os.path.join(curr_dir, folder))):
        if filename.endswith(".txt"):
            label_filesname = os.path.join(curr_dir, folder, filename)
            shutil.move(label_filesname, save_label_dir)
        else:
            image_filename = os.path.join(curr_dir, folder, filename)
            shutil.move(image_filename, save_imgs_dir)
            


def main():
    args = build_argparser().parse_args()
    #current directory
    dataset_name = args.dataset
    curr_dir = dataset_name #os.path.join("yolo_dataset", dataset_name)
    #move directory 
    labels_dir = "labels"
    imgs_dir = "images" 

    #create move_dir
    save_labels = os.path.join(curr_dir, labels_dir)
    save_imgs = os.path.join(curr_dir, imgs_dir)
    os.makedirs(save_labels, exist_ok=True)
    os.makedirs(save_imgs, exist_ok=True)

    for folder in sorted(os.listdir(curr_dir)):
        if folder == "train":
            process(curr_dir, folder, save_labels, save_imgs)
        elif folder == "val":
            process(curr_dir, folder, save_labels, save_imgs)
    
    os.rmdir(os.path.join(curr_dir, "train"))
    os.rmdir(os.path.join(curr_dir, "val"))



if __name__ == '__main__':
    sys.exit(main() or 0)