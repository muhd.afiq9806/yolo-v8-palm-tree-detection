import yaml
import argparse
import os

parser = argparse.ArgumentParser(description='Create a .yaml file.')
parser.add_argument(
        '-d', '--dataset_name',
        help='Dataset name.',
        type=str,
        default=os.getcwd()
)
parser.add_argument(
        '-nc', '--number_class',
        help='How many class.',
        type=int,
        default=os.getcwd()
)
parser.add_argument(
        '-c', '--class_name',
        help='List of class name.',
        type=list,
        default=os.getcwd()
)

args = parser.parse_args()

#Declare variable from argparse
dataset_name = args.dataset_name
num_cls = args.number_class
cls_name = args.class_name

data = {
    'train': dataset_name+'/images/train/',
    'val': dataset_name+'/images/val/',
    'nc': num_cls,
    'names': cls_name
}

with open(dataset_name+'.yaml', 'w') as file:
    yaml.dump(data, file)
